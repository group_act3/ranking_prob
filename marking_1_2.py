class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks
    
    def __str__(self):
        return f"{self.name}"
    
    def __lt__(self, other):
        
        return self.marks > other.marks
    
    def __eq__(self, other):
        return self.name == other.name
    
    def __hash__(self):
        return hash(self.name)

def read_students_from_file(filename):
    students = []
    with open(filename, 'r') as file:
        line_number = 0
        for line in file:
            line_number += 1
            data = line.strip().split()
            
            
            if len(data) < 2:
                print(f"Skipping invalid line {line_number}: {line.strip()}")
                continue
            
            try:
                name = data[0]
                marks = list(map(int, data[1:]))  
                students.append(Student(name, marks))
            except ValueError:
                print(f"Skipping invalid line {line_number}: {line.strip()}")
    
    if not students:
        raise ValueError("No valid student data found in file")
    
    
    num_subjects = len(students[0].marks)
    for student in students:
        if len(student.marks) != num_subjects:
            raise ValueError("All students must have the same number of marks")
    
    return students

def rank_students(students):
    students_sorted = sorted(students)
    current_group = [students_sorted[0]]
    result = []
    
    for student in students_sorted[1:]:
        if student < current_group[-1]:
            current_group.append(student)
        else:
            result.append("<".join(str(s) for s in current_group))
            current_group = [student]
    
    result.append("<".join(str(s) for s in current_group))
    
    for line in result:
        print(line)

def main():
    filename = "marks_2.txt"  
    try:
        students = read_students_from_file(filename)
        rank_students(students)
    except ValueError as e:
        print(f"Error: {e}")


main()

