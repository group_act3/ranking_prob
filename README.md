# Student Ranking Program

This Python program allows you to read student data from a file, validate it, and rank students based on their marks in descending order. It utilizes object-oriented programming principles and file handling to achieve its functionality.

## Features

- **Student Class**: Defines a `Student` class with attributes for name and marks, along with comparison methods to facilitate sorting by marks.
  
- **Reading from File**: Reads student data from a specified file (`marks_2.txt` by default), validating each line and ensuring consistency in the number of marks across all students.
  
- **Ranking Students**: Sorts students based on their marks and prints them in groups where each group consists of students with equal or decreasing marks.

## Usage

1. **Input File**: Ensure your input file (`marks_2.txt`) is correctly formatted with each line containing a student's name followed by their marks separated by spaces.

2. **Executing the Program**:
   - Run `main()` to execute the program.
   - The program will read the input file, rank the students based on their marks, and print the ranked groups to the console.

3. **Error Handling**:
   - If the input file is empty or contains invalid data (e.g., non-integer marks), appropriate error messages will be displayed.
   - Ensure all students in the file have the same number of marks to avoid errors during execution.

## Example

Given an input file `marks_2.txt` with the following content:
```
A 12 14 16
B 5  6  7
C 17 20 23
D 2  40 12
E 3  41 13
F 7  8  9
G 4  5  6
```

The program will output:
```
G<B<F<A<C
D<E
```

## Requirements

- Python 3.x
- No additional Python packages are required beyond the standard library.
